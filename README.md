# CrawlerDebout

### Version
0.2.0

Un script nodejs pour télécharger les anciennes diffusions des profiles periscopes.

### Pré-requis
- NodeJs: https://nodejs.org/
- livestreamer: http://docs.livestreamer.io/install.html

### Installation
```sh
git clone git@git.framasoft.org:Sobralia/CrawlerDebout.git
npm install
```

### Utilisation
Ajouter les profiles periscope dont vous souhaitez récupérer les video dans le fichier sources.txt. Une adresse par ligne.

```
https://www.periscope.tv/foo
https://www.periscope.tv/bar
...
``
