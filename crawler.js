var	request = require('request');
var	cheerio = require('cheerio');
var	fs = require('fs');
var exec = require('child_process').exec;
var	events = require('events');
var	eventEmitter = new events.EventEmitter();
var readline = require('readline');
var sanitize = require("sanitize-filename");

var	profiles = [];

var	broadcasts_list = {
	broadcasts: [],
	populate: function(){
		var	_this = this;
		for (var i = 0; i < profiles.length; i++)
		{
			var	the_profile = profiles[i];
			console.log("Fetching " + the_profile + " past broadcasts");
			request(the_profile, function(error, response, body){
				if (error){
					console.log("Error while attempting to reach url \"" + profile_url + "\": " + error);
				}
				else
				{
					if (response.statusCode === 200){
						//Parse periscorpes historic
						var	$ = cheerio.load(body);
						var	raw = $("#user-broadcasts").attr('content');
						if (raw !== undefined)
						{
							fs.writeFile("rawlol", raw, function(){});
							var broadcasts_data = JSON.parse(raw);
							var	remote_broadcasts = broadcasts_data.broadcasts;

							console.log(remote_broadcasts.length);
							for (var i = 0; i < remote_broadcasts.length; i++){
								var	the_broadcast = remote_broadcasts[i];
								//TODO Check if file exist before adding broadcast to dl list
								_this.broadcasts.push(the_broadcast);
							}
						}
					}
				}
				if (i == profiles.length - 1)
					eventEmitter.emit('broadcasts_ready');
			});
		}
	},
	downloadOne: function(){
		var	broadcast = this.broadcasts.pop();
		var	id = broadcast.id;
		var	name = sanitize(broadcast.status);
		var	profile_name = broadcast.user_display_name;
		var	raw_date = broadcast.created_at;
		var	url = "https://api.periscope.tv/api/v2/accessVideoPublic?broadcast_id=" + id;
		var	path = __dirname + "/video/" + profile_name + "/" + name + "_" + id + ".ts";

		createVideoProfileDirectory(profile_name);
		fs.stat(path, function(err, stat){
			if (err == null){
				console.log(path + " exist, skipped");				
				eventEmitter.emit('broadcasts_ready');
			}else if (err.code == "ENOENT"){
				console.log("Downloading: " + name + "_" + id + ".ts");
				request(url, function(error, response, body){
					if (error){
						console.log("Error while retrieving broadcast info with token " + id + " : " + error);
					}
					else
					{
						if (response.statusCode === 200){
							var	info = JSON.parse(body);
							var	cookies = info.cookies;
							var	the_cookie = "CloudFront-Policy=" + cookies[0].Value + 
										"; CloudFront-Signature=" + cookies[1].Value + 
										"; CloudFront-Key-Pair-Id=" + cookies[2].Value;
							var cmd = "livestreamer --http-cookie \"" + the_cookie + "\" \"hls://" + info.replay_url + "\" best -o \"" + path + "\"";

							exec(cmd, function(error, stdout, stderr) {
								if (error){
									console.log("Error while recording: " + error);
								}
								console.log(path + "downloaded!");								
								eventEmitter.emit('broadcasts_ready');
							});
						}
					}
				});
			}else{
				console.log("Error: " + err.code);
				eventEmitter.emit('broadcasts_ready');
			}
		});
	},
	next: function(){
			console.log(this.broadcasts.length + " in queue");
		if (this.broadcasts.length > 0){
			this.downloadOne();
		}
	}
}

function	createVideoProfileDirectory(profile_name){
	var	path = __dirname + "/video/" + profile_name;

	if (!fs.existsSync(path)){
    	fs.mkdirSync(path);
	}
}

//Create Video folder if not exist
if (!fs.existsSync(__dirname + "/video")){
	fs.mkdirSync(__dirname + "/video");
}

eventEmitter.on('broadcasts_ready', function(){
	broadcasts_list.next();
});

//Read sources from file
fs.stat(__dirname + "/sources.txt", function(err, stat){
	if (err === null)
	{
		readline.createInterface({
		    input: fs.createReadStream("sources.txt"),
		    terminal: false
		}).on('line', function(line) {
			profiles.push(line);
		}).on('close', function(){
			broadcasts_list.populate();			
		});
	}
});

